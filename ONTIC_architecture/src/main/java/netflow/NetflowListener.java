package netflow;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import akka.actor.Props;
import akka.routing.FromConfig;
import general.Listener;

/*
 * Listens in a port for netflow v5 and sends it to a parser
 */
public class NetflowListener extends Listener{

	private DatagramSocket socket;
	private byte[] netflow_bytes;
	private DatagramPacket netflow;
	
	@Override
	protected void connectToSource( String nada ) {
				//###############################################
				//# Open a UDP socket on given port             #
				//###############################################
		    	try{		
		    		this.socket = new DatagramSocket( config.getInt( "netflowport" ) );
		    	}catch(SocketException e){
		    		log.error( e.toString() );
		    	}
		    	this.netflow_bytes = new byte[ 1464 ];
		    	this.netflow = new DatagramPacket( netflow_bytes, netflow_bytes.length );
	}
	@Override
	protected void connectToDest( String type ) {
		out = getContext().actorOf( FromConfig.getInstance().props( Props.create( NetflowParser.class, type ) ), "netflow_parsers" );
	}
	
	public NetflowListener( String type ){
		//connect to destination and source
		super( type );
		log.info( "NetflowListener ha nacido "+cluster.selfAddress() );
	}

	@Override
	public void preStart() {
		doWork( ":)" );
	}
	@Override
	public void postRestart( Throwable reason ) {
		  preStart();
	}
	
	@Override
	protected Object read() {
		try{
			this.socket.receive( this.netflow );
		}
		catch(IOException e){ 
			log.error("Error receiving netflow"); 
		}
		return this.netflow_bytes;
	}

	@Override
	protected void send( Object msg ) {
			out.tell( ( byte[] )msg, getSelf() );
	}

	@Override
	public void onReceive(Object arg0) throws Exception {
		doWork( arg0 );
	}
}
