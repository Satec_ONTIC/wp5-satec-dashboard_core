package netflow;

import java.util.Date;

import akka.actor.Props;
import akka.routing.FromConfig;
import general.Processor;

/*
 * Filters each flow in the Netflow objects to add new fields
 */
public class NetflowFilter extends Processor{

	private RelevantNetflow rn;
	
	@Override
	protected void connectToDest( String type ) {
		out = getContext().actorOf( FromConfig.getInstance().props( Props.create( ElasticWriter.class, type ) ), "netflow_writers" );
	}
	public NetflowFilter( String type) {
		//connect to destination
		super( type );
		log.info( "NetflowFilter ha nacido "+cluster.selfAddress() );
	}

	@Override
	protected void send( Object msg ) {
		out.tell( ( RelevantNetflow )msg, getSelf() );	

	}

	@Override
	protected void doWork( Object o ) {
		Netflow n = ( Netflow )o;
		for(Netflow.Flow f : n.flows){

			Date tmpFirst = new Date ((n.unix_secs*1000)+(n.unix_nsecs/1000000)-(n.sys_uptime-f.first));
			Date tmpLast = new Date ((n.unix_secs*1000)+(n.unix_nsecs/1000000)-(n.sys_uptime-f.last));
			
			rn = new RelevantNetflow( f.srcaddr, f.dstaddr, f.dPkts, f.dOctets, tmpFirst, tmpLast, f.srcport, f.dstport, f.tcp_flags, f.prot, f.tos );
			rn.putConv( f.srcaddr.getHostAddress()+"_"+ f.dstaddr.getHostAddress() );
			rn.putFlow(f.srcaddr.getHostAddress()+"_"+ f.dstaddr.getHostAddress()+"_"+f.srcport+"_"+f.dstport+"_"+f.prot);
			send( rn );
		}
	}

	@Override
	public void onReceive( Object arg0 ) throws Exception {
		if( arg0 instanceof Netflow ){
			doWork( arg0 );
		}else{
			unhandled( arg0 );
		}
	}
}
