package netflow;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.Date;

public class RelevantNetflow implements Serializable{
	
	private static final long serialVersionUID = 3L;
	public InetAddress srcaddr;
	public InetAddress dstaddr;
	public int dPkts;
	public int dOctets;
	public Date first;
	public Date last;
	public int srcport;
	public int dstport;
	public byte tcp_flags;
	public byte prot;
	public int tos;
	public String conversation;
	public String flow;
	
	public RelevantNetflow( InetAddress sa, InetAddress da, int p, int b, Date f, Date l, int sp, int dp, byte tf, byte pr, int t ){
		this.srcaddr = sa;
		this.dstaddr = da;
		this.dPkts = p;
		this.dOctets = b;
		this.first =f;
		this.last = l;
		this.srcport = sp;
		this.dstport = dp;
		this.tcp_flags = tf;
		this.prot = pr;
		this.tos = t;
	}
	public void putConv( String c ){
		this.conversation = c;
	}
	
	public void putFlow( String c ){
		this.flow = c;
	}


	@Override
    public String toString() {
		final StringBuilder sb = new StringBuilder();
	    sb.append( "srcaddr="+srcaddr ).append(" ");
	    sb.append("dstaddr="+dstaddr).append(" ");
	    sb.append("dPkts="+dPkts).append(" ");
	    sb.append("dOctets="+dOctets).append(" ");
	    sb.append("first="+first).append(" ");
	    sb.append("last="+last).append(" ");
	    sb.append("srcport="+ srcport).append(" ");
	    sb.append("dstport="+dstport).append(" ");
	    sb.append("tcp_flags="+tcp_flags).append(" ");
        sb.append( prot ).append(" ");
		sb.append( tos ).append(" ");
		sb.append( conversation ).append(" ");
	    sb.append("\n");
        return sb.toString();
    }
}
