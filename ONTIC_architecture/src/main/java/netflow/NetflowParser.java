package netflow;

import akka.actor.Props;
import akka.routing.FromConfig;
import general.Processor;

/*
 * Receibes netflow v5 byte arrays and parses it to an object. Then the netflow object
 * is sent to a Filter
 */
public class NetflowParser extends Processor{
	private Netflow nf;
	
	@Override
	protected void connectToDest( String type ) {
		out = getContext().actorOf( FromConfig.getInstance().props( Props.create( NetflowFilter.class, type ) ), "netflow_filters" );
	}
	
	public NetflowParser( String type ) {
		// connect to destination
		super( type );
		log.info( "NetflowParser ha nacido "+cluster.selfAddress() );
	}

	@Override
	protected void doWork( Object o ) {
		byte[] ba = ( byte[] )o;
		nf = new Netflow( ba );
	}

	@Override
	public void onReceive( Object arg0 ) throws Exception{
		if( arg0 instanceof byte[] ){
			doWork( arg0 );
			send( this.nf );
		}else{
			unhandled( arg0 );
		}
	}

	@Override
	protected void send( Object msg ) {
		out.tell( (Netflow)msg, getSelf() );
	}
}
