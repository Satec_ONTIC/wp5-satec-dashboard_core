package netflow;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class Netflow implements Serializable{

	private static final long serialVersionUID = 1L;
	private static final int HEADERBYTES = 24;
	private static final int FLOWBYTES = 48;
	
	public class Flow implements Serializable{
		private static final long serialVersionUID = 1L;
		public InetAddress srcaddr;
		public InetAddress dstaddr;
		public int nexthop;
		public short input;
		public short output;
		public int dPkts;
		public int dOctets;
		public long first;
		public long last;
		public int srcport;
		public int dstport;
		public byte tcp_flags;
		public byte prot;
		public int tos;
		public short src_as;
		public short dst_as;
		public byte src_mask;
		public byte dst_mask;
		
		public Flow( ByteArrayInputStream f, int base ){
			getInt( f, "SRCADDR", base );
			getInt( f, "DSTADDR", base );
			getInt( f, "NEXTHOP", base );
			getShort( f, "INPUT", base );
			getShort( f, "OUTPUT", base );
			getInt( f, "DPKTS", base );
			getInt( f, "DOCTECTS", base );
			getInt( f, "FIRST", base );
			getInt( f, "LAST", base );
			getShort( f, "SRCPORT", base );
			getShort( f, "DSTPORT", base );
			getByte( f, "PAD1", base );
			getByte( f, "TCP_FLAGS", base );
			getByte( f, "PROT", base );
			getByte( f, "TOS", base );
			getShort( f, "SRC_AS", base );
			getShort( f, "DST_AS", base );
			getByte( f, "SRC_MASK", base );
			getByte( f, "DST_MASK", base );
			getShort( f, "PAD2", base );
		}
		private void getInt( ByteArrayInputStream bInput, String prop, int base ){
			byte[] r4 = new byte[4];
			if( prop.equals("SRCADDR") ){
				bInput.read( r4, 0, 4 );
				try{
					this.srcaddr = InetAddress.getByAddress( r4 );
				}catch(UnknownHostException e){}
			}else if( prop.equals("DSTADDR") ){
				bInput.read( r4, 0, 4 );
				try{
					this.dstaddr = InetAddress.getByAddress( r4 );
				}catch(UnknownHostException e){}
			}else if( prop.equals("NEXTHOP") ){
				bInput.read( r4, 0, 4 );
				this.nexthop = ByteBuffer.wrap( r4 ).getInt();
			}else if( prop.equals("DPKTS") ){
				bInput.read( r4, 0, 4 );
				this.dPkts = ByteBuffer.wrap( r4 ).getInt();
			}else if( prop.equals("DOCTECTS") ){
				bInput.read( r4, 0, 4 );
				this.dOctets = ByteBuffer.wrap( r4 ).getInt();
			}else if( prop.equals("FIRST") ){
				bInput.read( r4, 0, 4 );
				this.first = ByteBuffer.wrap( r4 ).getInt();
			}else if( prop.equals("LAST") ){
				bInput.read( r4, 0, 4 );
				this.last = ByteBuffer.wrap( r4 ).getInt();
			}
		}
		private void getShort( ByteArrayInputStream bInput, String prop, int base ){
			byte[] r2 = new byte[2];
			if( prop.equals("INPUT") ){
				bInput.read( r2, 0, 2 );
				this.input = ByteBuffer.wrap( r2 ).getShort();
			}else if( prop.equals("OUTPUT") ){
				bInput.read( r2, 0, 2 );
				this.output = ByteBuffer.wrap( r2 ).getShort();
			}else if( prop.equals("SRCPORT") ){
				bInput.read( r2, 0, 2 );
				this.srcport = ByteBuffer.wrap( r2 ).getShort() & 0xffff;
			}else if( prop.equals("DSTPORT") ){
				bInput.read( r2, 0, 2 );
				this.dstport = ByteBuffer.wrap( r2 ).getChar() & 0xffff;
			}else if( prop.equals("SRC_AS") ){
				bInput.read( r2, 0, 2 );
				this.src_as = ByteBuffer.wrap( r2 ).getShort();
			}else if( prop.equals("DST_AS") ){
				bInput.read( r2, 0, 2 );
				this.dst_as = ByteBuffer.wrap( r2 ).getShort();
			}else if( prop.equals("PAD2") ){
				bInput.read( r2, 0, 2 );
			}
		}
		private void getByte( ByteArrayInputStream bInput, String prop, int base ){
			byte[] r1 = new byte[1];
			byte[] r4 = {0,0,0,0};
			if( prop.equals("PAD1") ){
				bInput.read( r1, 0, 1 );
			}else if( prop.equals("TCP_FLAGS") ){
				bInput.read( r1, 0, 1 );
				this.tcp_flags = r1[0];
			}else if( prop.equals("PROT") ){
				bInput.read( r1, 0, 1 );
				this.prot = r1[0];
			}else if( prop.equals("TOS") ){
				bInput.read( r1, 0, 1 );
//				this.tos = r1[0];
				r4[3]=r1[0];
				this.tos =ByteBuffer.wrap( r4 ).getInt();
			}else if( prop.equals("SRC_MASK") ){
				bInput.read( r1, 0, 1 );
				this.src_mask = r1[0];
			}else if( prop.equals("DST_MASK") ){
				bInput.read( r1, 0, 1 );
				this.dst_mask = r1[0];
			}
		}
		
		@Override
	    public String toString() {
			final StringBuilder sb = new StringBuilder("");
			sb.append(srcaddr.getHostAddress()).append(" ");
	        sb.append(dstaddr.getHostAddress()).append(" ");
	       // sb.append("nexthop="+nexthop).append(" ");
//	        sb.append("input="+input).append(" ");
//	        sb.append("output="+output).append(" ");
	        sb.append(dPkts).append(" ");
	        sb.append(dOctets).append(" ");
	        sb.append(first).append(" ");
	        sb.append(last).append(" ");
	        sb.append(srcport).append(" ");
	        sb.append(dstport).append(" ");
	        sb.append(tcp_flags).append(" ");
	        sb.append(prot).append(" ");
	        sb.append(tos).append(" ");
//	        sb.append("src_as="+src_as).append(" ");
//	        sb.append("dst_as="+dst_as).append(" ");
//          sb.append("src_mask="+src_mask).append(" ");
//	        sb.append("dst_mask="+dst_mask).append(" ");
	        return sb.toString();
	    }
		
	}

	public short version;
	public short count;
	public long sys_uptime;
	public long unix_secs;
	public long unix_nsecs;
	public long flow_sequence;
	public byte engine_type;
	public byte engine_id;
	public short sampling_interval;
	public Flow[] flows;
	
	public Netflow( byte[] nf ){
		ByteArrayInputStream bInput = new ByteArrayInputStream( nf );
		getShort( bInput, "VERSION" );
		getShort( bInput, "COUNT" );
		getInt( bInput, "SYS_UPTIME" );
		getInt( bInput, "UNIX_SECS" );
		getInt( bInput, "UNIX_NSECS" );
		getInt( bInput, "FLOW_SEQUENCE" );
		getByte( bInput, "ENGINE_TYPE" );
		getByte( bInput, "ENGINE_ID" );
		getShort( bInput, "SAMPLING_INTERVAL" );
		this.flows = new Flow[ this.count ];
		for( int i=0; i<this.count; i++ ){
			this.flows[ i ] = new Flow( bInput, (HEADERBYTES + i*FLOWBYTES) );
		}
	}

	private void getShort( ByteArrayInputStream bInput, String prop ){
		byte[] r2 = new byte[2];
		if( prop.equals("VERSION") ){
			bInput.read( r2, 0, 2 );
			this.version = ByteBuffer.wrap( r2 ).getShort();
		}else if( prop.equals("COUNT") ){
			bInput.read( r2, 0, 2 );
			this.count = ByteBuffer.wrap( r2 ).getShort();
		}else if( prop.equals("SAMPLING_INTERVAL") ){
			bInput.read( r2, 0, 2 );
			this.sampling_interval = ByteBuffer.wrap( r2 ).getShort();
		}
	}
	private void getInt( ByteArrayInputStream bInput, String prop ){
		byte[] r4 = new byte[4];
		if( prop.equals("SYS_UPTIME") ){
			bInput.read( r4, 0, 4 );
			this.sys_uptime = ByteBuffer.wrap( r4 ).getInt();
		}else if( prop.equals("UNIX_SECS") ){
			bInput.read( r4, 0, 4 );
			this.unix_secs = ByteBuffer.wrap( r4 ).getInt();
		}else if( prop.equals("UNIX_NSECS") ){
			bInput.read( r4, 0, 4 );
			this.unix_nsecs = ByteBuffer.wrap( r4 ).getInt();
		}else if( prop.equals("FLOW_SEQUENCE") ){
			bInput.read( r4, 0, 4 );
			this.flow_sequence = ByteBuffer.wrap( r4 ).getInt();
		}
	}
	private void getByte( ByteArrayInputStream bInput, String prop ){
		byte[] r1 = new byte[1];
		if( prop.equals("ENGINE_TYPE") ){
			bInput.read( r1, 0, 1 );
			this.engine_type = r1[0];
		}else if( prop.equals("ENGINE_ID") ){
			bInput.read( r1, 0, 1 );
			this.engine_id = r1[0];
		}
	}
	public void clear() {
		version = count = sampling_interval = 0;
		sys_uptime = unix_secs = unix_nsecs = flow_sequence = 0;
		engine_type = engine_id = 0;
		Arrays.fill( flows, null );
	}
	
	@Override
    public String toString() {
		final StringBuilder sb = new StringBuilder();
	        sb.append( "version="+version ).append(" ");
	        sb.append("count="+count).append(" ");
	        sb.append("sys_uptime="+sys_uptime).append(" ");
	        sb.append("secs="+unix_secs).append(" ");
	        sb.append("nsecs="+unix_nsecs).append(" ");
	        sb.append("flow_seq="+flow_sequence).append(" ");
	        sb.append("et="+ engine_type).append(" ");
	        sb.append("ei="+engine_id).append(" ");
	        sb.append("si="+sampling_interval).append(" ");
			sb.append( sys_uptime ).append(" ");
			sb.append( unix_secs ).append(" ");
			sb.append( unix_nsecs ).append(" ");
			for( int i=0; i<this.count; i++ ){
				 sb.append( flows[i].toString() );
			}
			
	        sb.append("\n");
        return sb.toString();
    }

}