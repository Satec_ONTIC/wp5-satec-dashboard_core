package netflow;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;
import org.json.simple.JSONObject;

import general.Writer;

public class ElasticWriter extends Writer{

	protected int PORT;
	protected Client client;
	protected BulkProcessor bulkProcessor;
	protected String ecluster;
	protected String host;
	protected String index;
	protected String type;
	protected int bActs;
	protected int flushi;

	protected void defineType( String t ){
		this.type = config.getString( t+"type" );
	}

	@Override
	protected void connectToDest( String type ){
		defineType( type );
		this.PORT = config.getInt( "esport" );
		this.ecluster = config.getString( "escluster" );
		this.host = config.getString( "eshost" );
		this.index = config.getString( "esindex" );
		this.bActs = config.getInt( "esbulk" );
		this.flushi = config.getInt( "esflush" );
		try{
			initElasticClient();
		}catch( UnknownHostException e ){
			log.error( "initElasticClient failed" );
		}
		initBulkProcessor();
	}
	public ElasticWriter( String type ){
		super( type );
		log.info( "ElasticWriter ha nacido en "+cluster.selfAddress() );
	}

	protected void initElasticClient() throws UnknownHostException {
		Settings settings = Settings.settingsBuilder().put( "cluster.name", this.ecluster ).build();
		client = TransportClient.builder().settings( settings ).build()
				.addTransportAddress( new InetSocketTransportAddress(InetAddress.getByName( this.host ), this.PORT));
	}
	protected void initBulkProcessor() {
		bulkProcessor = BulkProcessor.builder( client, new BulkProcessor.Listener() {
			public void afterBulk(long arg0, BulkRequest arg1, BulkResponse arg2) {
				if(arg2.hasFailures()){
				}
			}
			public void afterBulk(long arg0, BulkRequest arg1, Throwable arg2) {
				if(arg2.getMessage()!=null){
				}
			}
			public void beforeBulk(long arg0, BulkRequest arg1){
			}
		}).setBulkActions( this.bActs ).setBulkSize( new ByteSizeValue( 20, ByteSizeUnit.MB ) )
				.setFlushInterval( TimeValue.timeValueSeconds( this.flushi ) ).setConcurrentRequests( 1 ).build();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void send( Object msg ) {
		JSONObject objaa = null;
		if( msg instanceof RelevantNetflow ){
			RelevantNetflow relnet = (RelevantNetflow)msg;
			objaa = new JSONObject();
			objaa.put( "netflowipv4srcaddr", relnet.srcaddr.getHostAddress());
			objaa.put( "netflowipv4dstaddr", relnet.dstaddr.getHostAddress());
			objaa.put( "netflowinpkts", relnet.dPkts );
			objaa.put( "netflowinbytes", relnet.dOctets );
			objaa.put( "netflowfirstswitched", relnet.first );
			objaa.put( "netflowlastswitched", relnet.last );
			objaa.put( "netflowl4srcport", relnet.srcport );
			objaa.put( "netflowl4dstport", relnet.dstport );
			objaa.put( "netflowtcpflags", relnet.tcp_flags );
			objaa.put( "netflowprotocol", relnet.prot );
			objaa.put( "netflowsrctos", relnet.tos );
			//Adding...
			objaa.put( "conversation", relnet.conversation );
			objaa.put( "flow", relnet.flow );
		}else if ( msg instanceof String ){
			String pcap = (String)msg;
			objaa = new JSONObject();
			objaa.put( "netflowpcapfile", pcap.trim() );
		}else if ( msg instanceof Map<?,?> ){
			Map<String,String> csv = (Map<String,String>)msg;
			objaa = new JSONObject( csv );
		}
		try{
			bulkdata( objaa );
		}catch( IOException e ){
			log.error( "bulkdata failed" );
		}
	}

	@Override
	public void onReceive(Object arg0) throws Exception {
		if( arg0 instanceof RelevantNetflow ){
			send( arg0 );
		}else if (arg0 instanceof String) {
			send( arg0 );
		}else if (arg0 instanceof Map<?,?>) {
			send( arg0 );
		} else{
			unhandled( arg0 );
		}
	}

	protected void bulkdata(Object data) throws IOException {
		if ( data instanceof String )
			bulkProcessor.add( new IndexRequest( this.index, this.type ).source( (String)data ) );
		else if( data instanceof JSONObject )
			bulkProcessor.add( new IndexRequest( this.index, this.type ).source( (JSONObject)data ) );
	}
}
