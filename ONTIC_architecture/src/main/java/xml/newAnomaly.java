package xml;

import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class newAnomaly {
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	private Date start;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	private Date end;
	private List <Inet4Address> srcpoints = new ArrayList<Inet4Address>();
	private List <Inet4Address> dstpoints = new ArrayList<Inet4Address>();
	private List <String> protocols = new ArrayList<String>();
	private String typeanomaly;
	ArrayList<Integer> srcports = new ArrayList<Integer>();
	ArrayList<Integer> dstports = new ArrayList<Integer>();
	private int important;
	private long id;


 public newAnomaly (Date start, Date end){
	 this.start = start;
	 this.end = end;
 }
 
 public newAnomaly (){
	
 }
 
	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}
	
	public List<Inet4Address> getSrcpoints() {
		return srcpoints;
	}

	public void setIpSrc(List<Inet4Address> ipSrc) {
		this.srcpoints = ipSrc;
	}

	public List<Inet4Address> getDstpoints() {
		return dstpoints;
	}

	public void setIpDst(List<Inet4Address> ipDst) {
		this.dstpoints = ipDst;
	}

	public List<String> getProtocols() {
		return protocols;
	}

	public void setProtocols(List<String> protocols) {
		this.protocols = protocols;
	}

	public String getTypeanomaly(){
		return typeanomaly;
	}

	public void setTypeanomaly(String typeanomaly){
		this.typeanomaly = typeanomaly;
	}

	public ArrayList<Integer> getSrcports() {
		return srcports;
	}

	public void setSrcPort(ArrayList<Integer> srcPort) {
		this.srcports = srcPort;
	}

	public ArrayList<Integer> getDstports() {
		return dstports;
	}

	public void setDstPort(ArrayList<Integer> dstPort) {
		this.dstports = dstPort;
	}

	public int getImportant() {
		return important;
	}

	public void setFidelity(int fidelity) {
		this.important = fidelity;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String toString(){
		return  "Id: " + this.getId() + "\n" + 
				"Start: " + this.getStart() + "\n" +
				"End: " + this.getEnd() + "\n" +
				"Type anomaly: " +this.getTypeanomaly() + "\n" +
				"Ips Source: "+ this.getSrcpoints() +"\n" +
				"Ips Destination: "+this.getDstpoints() + "\n" +
				"Src Ports: " + this.getSrcports() + "\n" +
				"Dst Ports: " + this.getDstports() + "\n" + 
				"Protocols " + this.getProtocols() + "\n" +
				"Fidelity: "+ this.getImportant() + "\n";
	}

	@Override
	public boolean equals( Object obj ){
		boolean ret = true;
		newAnomaly other = (newAnomaly)obj;
		List<Inet4Address> one = other.getSrcpoints();
		if( one.size()==this.srcpoints.size() ){
			for( Inet4Address inet : one ){
				if( !this.srcpoints.contains( inet ) ){
					ret = false;
					break;
				}
			}
			if( ret==true ){
				List<Inet4Address> two = other.getDstpoints();
				if( two.size()==this.dstpoints.size() ){
					for( Inet4Address inet : two ){
						if( !this.dstpoints.contains( inet ) ){
							ret = false;
							break;
						}
					}
					if( ret==true ){
						String typo = other.getTypeanomaly();
						if( ( typo.contains( "ATTACK" ) && !this.typeanomaly.contains( "ATTACK" ) ) ||
							( typo.contains( "RECOGNITION" ) && !this.typeanomaly.contains( "RECOGNITION" ) ) ||
							( typo.contains( "TO MONITOR" ) && !this.typeanomaly.contains( "TO MONITOR" ) ) ||
							( typo.contains( "unknown" ) && !this.typeanomaly.contains( "unknown" ) ) ){
							ret = false;
						}
					}
				}else{
					ret = false;
				}
			}
		}else{
			ret = false;
		}
		return ret;
	}
}
