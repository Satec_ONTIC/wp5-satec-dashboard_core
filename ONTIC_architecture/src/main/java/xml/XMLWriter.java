package xml;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import netflow.ElasticWriter;

public class XMLWriter extends ElasticWriter{

	private ObjectMapper mapper = new ObjectMapper();
	private SNMPAgent snmp = new SNMPAgent( config.getString( "snmphost" ) );
	private String toa;

	public XMLWriter( String type ){
		super( type );
	}

	@Override
	protected void send(Object msg){
		String obj1 = (String) msg;
		try {
			bulkdata( obj1 );
		} catch (IOException e){
			log.error("bulkdata failed");
		}
		if( toa.contains( "ATTACK" ) )
			snmp.sendTrap_Version1( 3 );
		else if( toa.contains( "TO MONITOR" ) )
			snmp.sendTrap_Version1( 2 );
		else
			snmp.sendTrap_Version1( 1 );
	}

	@Override
	public void onReceive(Object arg0) throws Exception{
		if (arg0 instanceof newAnomaly) {
			newAnomaly na = (newAnomaly)arg0;
			this.toa = na.getTypeanomaly();
			send(mapper.writeValueAsString(arg0));
		} else {
			unhandled(arg0);
		}
	}
}
