package xml;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import akka.actor.Props;
import general.Listener;

public class FolderListener extends Listener{
	
	protected WatchService watchService;

	public FolderListener( String folder) {
		super( folder );
		log.info( "FolderListener ha nacido "+cluster.selfAddress() );
	}

	
	@Override
	public void preStart() {
		doWork( ":)");
	}
	@Override
	public void postRestart(Throwable reason) {
		  preStart();
		}

	protected void createWatch( Path path ) {
		try {
			watchService = path.getFileSystem().newWatchService();
			path.register(watchService, new WatchEvent.Kind[] { ENTRY_CREATE });
		} catch (IOException e) {
			log.error("Creating watchService, probably path to watch is wrong"+ e);
			System.exit(1);
		}
	}
	@Override
	protected Object read() {
		WatchKey key = null;
		try {
			key = watchService.take();
		} catch (InterruptedException e) {
			log.error("Handle watchService, event wrong"+ e);
		}
		return key;
	}

	@Override
	protected void send(Object msg) {
		WatchKey key = (WatchKey) msg;
		if (key != null) {
			for (WatchEvent<?> event : key.pollEvents()){
				Path dir = (Path) key.watchable();
				Path fullPath = dir.resolve((Path) event.context());
				File inputFile =  fullPath.toFile();
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = null;
				try {
					dBuilder = dbFactory.newDocumentBuilder();
				} catch (ParserConfigurationException e) {
					log.error("Creating DocumentBuilder class"+ e);
				}
				Document doc = null;
				try {
					if (inputFile.canRead()){
						doc = dBuilder.parse(inputFile);
					}
				} catch (SAXException e) {
					log.error("Parising document "+ e);
				} catch (IOException e) {
						try {
							Thread.sleep(30);
							doc = dBuilder.parse(inputFile);
						} catch (SAXException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					} catch (Exception e){
						try {
							Thread.sleep(30);
							doc = dBuilder.parse(inputFile);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							log.error("One error was happen" + e);
						} catch (SAXException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							log.error("One error was happen" + e);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							log.error("One error was happen" + e);
						}
				}
				if (doc != null){
					out.tell( doc, getSelf() );
				}
			}
			key.reset();
		}		
	}

	@Override
	public void onReceive(Object arg0) {
		doWork(arg0);
	}


	@Override
	protected void connectToSource( String type ) {
		Path directoryToWatch = Paths.get( config.getString( type+"folder" ) );
		createWatch( directoryToWatch );	
	}


	@Override
	protected void connectToDest( String type ) {
		out = getContext().actorOf(Props.create( XMLParser.class, type ), type+"xmlparser" );
	}

}
