package xml;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledFuture;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import akka.actor.Props;
import general.Processor;

public class XMLParser extends Processor{

	private List<newAnomaly> listAnomalies = new ArrayList<newAnomaly>();
	private List<newAnomaly> sendAnomalies = new ArrayList<newAnomaly>();
	private List<newAnomaly> bufereadas = new ArrayList<newAnomaly>();
	private Timer timer = new Timer();
	private boolean waiting = false;
	private String type;

	@Override
	protected void connectToDest( String type ){
		out = getContext().actorOf(Props.create( XMLWriter.class, type ), type+"xmlwriter" );
	}
	
	public XMLParser( String type ) {
		super( type );
		log.info( "XMLParser ha nacido "+cluster.selfAddress() );
		this.type=type;
	}

	@Override
	protected void doWork( Object o ){
		if( type.equals( "simple" ) ){
			Document doc = (Document) o;
			doc.getDocumentElement().normalize();
			listAnomalies.clear();
			Date start = new Date((Long.parseLong(doc.getDocumentElement().getAttribute("start"))));
			Date end = new Date((Long.parseLong(doc.getDocumentElement().getAttribute("end"))));
			NodeList signaturesOfAnomalies = doc.getDocumentElement().getElementsByTagName("signaturesOfAnomalies");
			for( int temp = 0; temp < signaturesOfAnomalies.getLength(); temp++ ){
				Node nNode = signaturesOfAnomalies.item( temp );
				for ( int recAnomalias = 1; recAnomalias < nNode.getChildNodes().getLength(); recAnomalias += 2 ){
					newAnomaly anom = new newAnomaly( start, end );
					Node an = nNode.getChildNodes().item(recAnomalias);
					String[] points = an.getAttributes().getNamedItem( "point" ).getTextContent().split( "," );
					String[] dests = an.getAttributes().getNamedItem( "mainIPs" ).getTextContent().split( "," );
					List<Inet4Address> listSrc = new ArrayList<Inet4Address>();
					List<Inet4Address> listDst = new ArrayList<Inet4Address>();
					Inet4Address srcAddress=null;
					Inet4Address dstAddress=null;
					try{
						if( points.length > 0 )
							srcAddress = (Inet4Address) Inet4Address.getByName(points[0]);
						for( String dest : dests ){
							if( dest!=null && !dest.equals("") ){
								dstAddress = (Inet4Address) Inet4Address.getByName( dest );
								listDst.add( dstAddress );
							}
						}
					}catch( UnknownHostException e1 ){
						log.error( "Parsing src/dst Inet4Address"+ e1 );
					}
					listSrc.add( srcAddress );
					anom.setIpSrc( listSrc );
					anom.setIpDst( listDst );
					int dissim = (int)Float.parseFloat( an.getAttributes().getNamedItem( "dissim" ).getTextContent() );
					anom.setFidelity( dissim );
					String type= an.getAttributes().getNamedItem( "possAnom" ).getTextContent();
					anom.setTypeanomaly( type );
					listAnomalies.add( anom );
				}
				sendAnomalies.clear();
				for( newAnomaly nuevo : listAnomalies ){
					if( bufereadas.contains( nuevo ) ){
						newAnomaly viejito = bufereadas.get( bufereadas.indexOf( nuevo ) );
						nuevo.setStart( viejito.getStart() );
					}
				}
				for( newAnomaly viejo : bufereadas ){
					if( !listAnomalies.contains( viejo ) ){
						sendAnomalies.add( viejo );
					}
				}
				if( waiting ){
					waiting = false;
					timer.cancel();
					timer = new Timer();
				}
				bufereadas.clear();
				bufereadas.addAll( listAnomalies );
				timer.schedule( new TimerTask(){
		            public void run() {
		            	sendAnomalies.clear();
		            	sendAnomalies.addAll( bufereadas );
		            	send( bufereadas );
		                waiting = false;
		                bufereadas.clear();
		            }
		         }, 5000);
				waiting = true;
			}
		}else if( type.equals( "full" ) ){
//			Document doc = (Document) o;
//			doc.getDocumentElement().normalize();
//			listAnomalies.clear();
//			NodeList signatureOfAnAnomaly = doc.getDocumentElement().getElementsByTagName("signatureOfAnAnomaly");
//			for (int b = 0; b < signatureOfAnAnomaly.getLength(); b++){
//				newAnomaly a = new newAnomaly();
//				NodeList childs=null;
//				Node nNodee = signatureOfAnAnomaly.item(b);
//				Date start = new Date(Long.parseLong(nNodee.getAttributes().getNamedItem("starttime").getNodeValue()));
//				Date end = new Date(Long.parseLong(nNodee.getAttributes().getNamedItem("endtime").getNodeValue()));
//				a.setStart(start);
//				a.setEnd(end);
//				a.setId(Long.parseLong(nNodee.getAttributes().getNamedItem("id").getNodeValue()));
//				childs = nNodee.getChildNodes();
//				for (int c = 1; c < childs.getLength(); c += 2) {
//					Node nNodeRule = childs.item(c);
//						if ( nNodeRule.getNodeName().equals("srcpoints") ) {
//							NodeList srcPoints = nNodeRule.getChildNodes();
//							List<Inet4Address> listSrc = new ArrayList<Inet4Address>();
//							for (int r = 1; r < srcPoints.getLength(); r += 2) {
//								Node srcPoint = srcPoints.item(r);
//								String aux = srcPoint.getTextContent();
//								try{
//									Inet4Address address = (Inet4Address) Inet4Address.getByName(aux);
//									listSrc.add(address);
//								}catch(UnknownHostException e){
//									break;
//								}
//							}
//							a.setIpSrc(listSrc);
//						} else if ( nNodeRule.getNodeName().equals("dstpoints") ) {
//							NodeList dstPoints = nNodeRule.getChildNodes();
//							List<Inet4Address> listDst = new ArrayList<Inet4Address>();
//							for (int r = 1; r < dstPoints.getLength(); r += 2) {
//								Node dstPoint = dstPoints.item(r);
//								String aux = dstPoint.getTextContent();
//								try{
//									Inet4Address address = (Inet4Address) Inet4Address.getByName(aux);
//									listDst.add(address);
//								}catch(UnknownHostException e){
//									break;
//								}
//							}
//							a.setIpDst(listDst);
//						} else if ( nNodeRule.getNodeName().equals("srcports") ) {
//							NodeList srcPorts = nNodeRule.getChildNodes();
//							ArrayList<Integer> listSrcPorts = new ArrayList<Integer>();
//							for (int r = 1; r < srcPorts.getLength(); r += 2) {
//								Node srcPort = srcPorts.item(r);
//								listSrcPorts.add( Integer.parseInt( srcPort.getTextContent() ) );
//							}
//							a.setSrcPort(listSrcPorts);
//						} else if ( nNodeRule.getNodeName().equals("dstports") ) {
//							NodeList dstPorts = nNodeRule.getChildNodes();
//							ArrayList<Integer> listDstPorts = new ArrayList<Integer>();
//							for (int r = 1; r < dstPorts.getLength(); r += 2) {
//								Node dstPort = dstPorts.item(r);
//								listDstPorts.add(Integer.parseInt(dstPort.getTextContent()));
//							}
//							a.setDstPort(listDstPorts);
//						} else if ( nNodeRule.getNodeName().equals("protocols") ) {
//							NodeList protocols = nNodeRule.getChildNodes();
//							ArrayList<String> listProtocols = new ArrayList<String>();
//							for (int r = 1; r < protocols.getLength(); r += 2) {
//								Node protocol = protocols.item(r);
//								listProtocols.add(protocol.getTextContent());
//							}
//							a.setProtocols(listProtocols);
//						} else if ( nNodeRule.getNodeName().equals("typeanomaly") ) {
//							a.setTypeanomaly(nNodeRule.getTextContent());
//						} else if ( nNodeRule.getNodeName().equals("important") ) {
//							a.setFidelity(Integer.parseInt(nNodeRule.getTextContent()));
//						}
//				}
//				listAnomalies.add(a);
//			}
		}
		
	}

	@Override
	public void onReceive( Object arg0 ) throws Exception {
		if( arg0 instanceof Document ){
			doWork( arg0 );
			send( this.sendAnomalies );
		}else{
			unhandled( arg0 );
		}
	}

	@Override
	protected void send( Object msg ) {
		for (newAnomaly l : sendAnomalies){
			out.tell( l, getSelf());
		}
	}
}
