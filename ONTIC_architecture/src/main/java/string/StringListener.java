package string;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import akka.actor.Props;
import general.Listener;
import netflow.ElasticWriter;

public class StringListener extends Listener{

	private DatagramSocket socket;
	private byte[] netflow_bytes;
	private DatagramPacket netflow;
	
	@Override
	protected void connectToSource( String nada ) {
		    	try{		
		    		this.socket = new DatagramSocket( config.getInt( "stringport" ) );
		    	}catch(SocketException e){
		    		log.error( e.toString() );
		    	}
		    	this.netflow_bytes = new byte[ 1464 ];
		    	this.netflow = new DatagramPacket( netflow_bytes, netflow_bytes.length );
	}
	@Override
	protected void connectToDest( String type ) {
		out = getContext().actorOf( Props.create( ElasticWriter.class, type ), "pcapfilewriter" );
	}
	
	public StringListener( String type ){
		//connect to destination and source
		super( type );
		log.info( "StringListener ha nacido "+cluster.selfAddress() );
	}

	@Override
	public void preStart() {
		doWork( ":)" );
	}
	@Override
	public void postRestart( Throwable reason ) {
		  preStart();
	}
	
	@Override
	protected Object read() {
		try{
			this.socket.receive( this.netflow );
		}
		catch(IOException e){ 
			log.error("Error receiving String"); 
		}
		return new String( this.netflow.getData() );
	}

	@Override
	protected void send( Object msg ) {
			out.tell( ( String )msg, getSelf() );
	}

	@Override
	public void onReceive(Object arg0) throws Exception {
		doWork( arg0 );
	}
}
