package general;


public abstract class Listener extends Processor{
	
	protected abstract void connectToSource( String c );
	public Listener( String c ){
		super( c );
		connectToSource( c );
	}
	protected abstract Object read();
	//Only public method
	@Override
	public void doWork( Object obj ){
		while( true ){
			send( read() );
    	}
	}
}
