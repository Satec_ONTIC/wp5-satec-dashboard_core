package general;

import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;
import akka.cluster.ClusterEvent.MemberEvent;
import akka.cluster.ClusterEvent.MemberRemoved;
import akka.cluster.ClusterEvent.MemberUp;
import akka.cluster.ClusterEvent.UnreachableMember;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class ClusterSubscriber extends UntypedActor{
	  private LoggingAdapter log;
	  private Cluster cluster;
//	  private ClusterMetricsExtension extension;
	 
	  
	  public ClusterSubscriber(){
		  log= Logging.getLogger( getContext().system(), this );
		  cluster = Cluster.get( getContext().system() );
//		  extension = ClusterMetricsExtension.get( getContext().system() );
	  }
	  //subscribe to cluster changes
	  @Override
	  public void preStart() {
	    //#subscribe
	    cluster.subscribe( getSelf(), ClusterEvent.initialStateAsEvents(), MemberEvent.class, UnreachableMember.class);
//	    extension.subscribe( getSelf() );
	    //#subscribed
	    log.info( "{} subscribed to cluster changes", getSelf() );
	  }
	 
	  //re-subscribe when restart
	  @Override
	  public void postStop() {
	    cluster.unsubscribe( getSelf() );
//	    extension.unsubscribe( getSelf() );
	  }
	 
	  @Override
	  public void onReceive( Object message ) {
	    if ( message instanceof MemberUp ) {
	      MemberUp mUp = (MemberUp) message;
	      log.info( "++++++++++++++++++++++++++++++++++++++++++Member is Up: {}", mUp.member() );
	 
	    } else if ( message instanceof UnreachableMember ) {
	      UnreachableMember mUnreachable = (UnreachableMember) message;
	      log.info( "Member detected as unreachable: {}", mUnreachable.member() );
	 
	    } else if ( message instanceof MemberRemoved ) {
	      MemberRemoved mRemoved = (MemberRemoved) message;
	      log.info( "-------------------------------------------Member is Removed: {}", mRemoved.member() );
	 
	    } else if ( message instanceof MemberEvent ) {
	      // ignore
//	    }else if ( message instanceof ClusterMetricsChanged ) {
//	    	ClusterMetricsChanged clusterMetrics = (ClusterMetricsChanged) message;
//	        for ( NodeMetrics nodeMetrics : clusterMetrics.getNodeMetrics() ) {
//	          if ( nodeMetrics.address().equals( cluster.selfAddress() ) ) {
//	            logHeap( nodeMetrics );
//	            logCpu( nodeMetrics );
//	          }
//	        }
//	   
//	      } else if ( message instanceof CurrentClusterState ) {
	        // Ignore.
	      } else {
	        unhandled(message);
	      }
	 
	  }
//	  void logHeap( NodeMetrics nodeMetrics ) {
//		    HeapMemory heap = StandardMetrics.extractHeapMemory( nodeMetrics );
//		    if (heap != null) {
//		      log.info("Used heap: {} MB", ( (double) heap.used() ) / 1024 / 1024);
//		    }
//		  }
//		 
//		  void logCpu( NodeMetrics nodeMetrics ) {
//		    Cpu cpu = StandardMetrics.extractCpu( nodeMetrics );
//		    if ( cpu != null && cpu.systemLoadAverage().isDefined() ) {
//		      log.info( "Load: {} ({} processors)", cpu.systemLoadAverage().get(), cpu.processors() );
//		    }
//		  }
}
