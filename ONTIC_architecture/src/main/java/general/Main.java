package general;

import java.util.HashMap;
import java.util.Map;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorSystem;
import akka.actor.Props;
import csv.CsvListener;
import netflow.NetflowListener;
import string.StringListener;
import xml.FolderListener;

/*
 * Parameters to Main: ["leader"|"seed"] akka.remote.netty.tcp.hostname
 */

public class Main {
	
	public static void main(String[] args) {
		if( args.length >= 2 ){
			if (args[0].contains("leader")) {
				// leader always listens on default port
				startup(new String[] { "2552" }, args[1]);
	
			} else if (args[0].contains("seed")) {
				// seed nodes listen always on port 2551
				startup(new String[] { "2551" }, args[1]);
	
			}
		}else{
			System.out.println( "arguments are missing, bitch!" );
		}
	}

	public static void startup(String[] ports, String ip){
		// Override the configuration of the port
		Map<String, String> prope = new HashMap<String, String>();
		for (String port : ports) {
			prope.put("akka.remote.netty.tcp.port", port);
			prope.put("akka.remote.netty.tcp.hostname", ip);
			Config config = ConfigFactory.parseMap(prope).withFallback(ConfigFactory.load());
			// Create an Akka system
			ActorSystem actorSystem = ActorSystem.create( "ActorSystem", config );
			if (port.equals( "2552" )){
				actorSystem.actorOf( Props.create( ClusterSubscriber.class ), "subscriber" );
				// Netflow init
				actorSystem.actorOf( Props.create( NetflowListener.class, "netflow" ), "netflow_listener" );
				actorSystem.actorOf( Props.create( StringListener.class, "netflow" ), "pcapfile_Listener" );
				// now, two types of xmls are supported: simple & full
				actorSystem.actorOf( Props.create( FolderListener.class, "simple" ), "simple_xml_listener" );
//				actorSystem.actorOf( Props.create( FolderListener.class, "full" ), "full_xml_listener" );
				actorSystem.actorOf( Props.create( CsvListener.class, "csv" ), "csv_listener" );
			}
		}
	}

}
