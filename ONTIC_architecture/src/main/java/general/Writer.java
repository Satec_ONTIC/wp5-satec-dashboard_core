package general;

import com.typesafe.config.Config;

import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public abstract class Writer extends UntypedActor{

	protected LoggingAdapter log;
	protected Config config;
	protected Cluster cluster;
	protected abstract void connectToDest( String s );
	public Writer( String s ){
		log = Logging.getLogger( getContext().system(), this );
		config = getContext().system().settings().config();
		cluster = Cluster.get( getContext().system() );
		connectToDest( s );
	}
	protected abstract void send( Object msg );
}
