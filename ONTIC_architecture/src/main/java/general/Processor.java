package general;

import akka.actor.ActorRef;

public abstract class Processor extends Writer{
	protected ActorRef out;
	public Processor( String c ){
		super( c );
	}
	protected abstract void doWork( Object o );
}
