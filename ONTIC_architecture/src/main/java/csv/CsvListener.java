package csv;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;

import akka.actor.Props;
import xml.FolderListener;

public class CsvListener extends FolderListener{

	public CsvListener(String folder) {
		super(folder);
		log.info( "CsvListener ha nacido "+cluster.selfAddress() );
	}

	@Override
	protected void send(Object msg) {
		WatchKey key = (WatchKey) msg;
		if (key != null) {
			for (WatchEvent<?> event : key.pollEvents()) {
				Path dir = (Path) key.watchable();
				Path fullPath = dir.resolve((Path) event.context());
				File inputFile =  fullPath.toFile();	
				if ( inputFile != null ){
					out.tell( inputFile, getSelf() );
				}
			}
			key.reset();
		}		
	}
	@Override
	protected void connectToDest( String type ) {
		out = getContext().actorOf(Props.create( CsvParser.class, type ), "csvparser" );
	}
}
