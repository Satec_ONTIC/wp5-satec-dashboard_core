package csv;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import akka.actor.Props;
import general.Processor;
import netflow.ElasticWriter;

public class CsvParser extends Processor{

	public CsvParser( String c ) {
		super(c);
		log.info( "CsvParser ha nacido "+cluster.selfAddress() );
	}

	@Override
	protected void doWork(Object o) {
		CsvMapper mapper = new CsvMapper();
		CsvSchema schema = CsvSchema.emptySchema().withHeader();
		MappingIterator<Map<String, String>> it = null;
		try {
			it = mapper.readerFor(Map.class).with(schema).readValues( (File)o );
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while ( it.hasNext() ) {
			Map<String, String> rowAsMap = it.next();
			send( rowAsMap );
		}
	}

	@Override
	protected void connectToDest( String s ) {
		out = getContext().actorOf(Props.create( ElasticWriter.class, s ), "csvwriter" );
	}

	@Override
	protected void send(Object msg) {
		out.tell( msg, getSelf());
	}

	@Override
	public void onReceive(Object arg0) throws Exception {
		if( arg0 instanceof File ){
			doWork( arg0 );
		}else{
			unhandled( arg0 );
		}
	}

}
