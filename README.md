The architecture of this Module is based in a "Fast Data Model" is made up of the following components:

�	A set of data traffic sources such as:
	o	ONTS (ONTIC Network Traffic Summary) data set [18][19]: PCAP files with captured traffic at raw packet level.
	o	Anomaly Detection Subsystem: XML files with information on the identified anomalies.
	o	Network hardware: NetFlow v5 records 

�	Network Traffic Data Collector module: scalable pool of pipelines to pre-process input data. Pipelines are commonly associated to data sources. The following pipelines are implemented:
	o	PCAP pipeline: It reads PCAP files and converts TCP/IP headers to flows in NetFlow format.
	o	NetFlow pipeline: It receives NetFlow records through a defined UDP port and pre-processes these records, for example, for creating tuples with new conversations, and stores the resulted data in the database.
	o	Anomaly detection pipeline: It receives XML files from the Anomaly Detection Subsystem (from a commonly agreed file directory or through a Web Service implemented in the Network Traffic Dashboard Subsystem), parses the XML files (e.g. to discriminate between new and previous anomalies that continue active) and stores the results in the database.

This is the code based on AKKA. 